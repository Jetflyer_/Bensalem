#include <iostream>
#include <string>
using namespace std;

int main()
{
    int error = 0;
    int choix = 0;   
    string Wanglais[20] = {"to browse the web","a chat room","to check one's e-mail","a computer","a computer scientist","e-commerce","to computerise","to connect to","to crack a code","a data bank","a desktop computer","to destroy","a developer","digital","to display","to download","to erase","a file","identity theft","a hard disk"};                            // définitions du tableau des prix 
    string Wfrancais[20] = {"naviger sur le web","un forum de discussion","consulter ses mails","un ordinateur","un informaticien","le commerce électronique","informatiser","se connecter à","percer à jour un code","une banque de données","un ordinateur de bureau","détruire","un développeur informatique","numérique","afficher","télécharger","effacer","un fichier","le vol d'identité","un disque dur"};                        // définitions du tableau des options 
    string Wanglais2[20];
    string Wfrancais2[20];
    std::cout<<"PRESS ENTER !"<<endl;
    cin.clear();                                        
    cin.ignore(20000, '\n');  
    std::cout<<"choisisez entre 1 et 2 !"<<endl;
    std::cout<<"1 . anglais ---> francais"<<endl;
    std::cout<<"2 . francais ---> anglais"<<endl;
    cin >> choix;
    if(choix == 1){
        std::cout << "quizz pour apprendre les mot en anglais !"<<endl;
        std::cout << "pour chaque mot ecrit donne sont equivalent exacte" << endl;
        std::cout << "-----------" << endl;
        for (int i = 0; i < 20; i++){          
            std::cout<< i + 1 <<" : "<< Wanglais[i] <<" ---> " ;
            getline(cin, Wfrancais2[i]);                       
        }
        std::cout << "=================="<<endl;
        std::cout << "r" << char(0x82) << "sultat : "<<endl;
        std::cout << "================= "<<endl;
        cin.ignore();
        for (int i = 0; i < 20; ++i){ 
            if(Wfrancais2[i] != Wfrancais[i]){
                error ++;
            }                  
            std::cout << Wanglais[i] << " ---> "<<"\" "<<Wfrancais2[i]<<" \""<< " R" << char(0x82) << "ponse : " << Wfrancais[i] <<endl;
        }
        std::cout <<"=-=-=-=-=-=-=-=-=-=-=-=-=-="<<endl;
        std::cout <<"Nombre d'erreur : "<< error <<endl;
    }
    else{
        std::cout << "quizz pour apprendre les mot en anglais !"<<endl;
        std::cout << "pour chaque mot ecrit donne sont equivalent exacte" << endl;
        std::cout << "-----------" << endl;
        for (int i = 0; i < 20; i++){          
            std::cout<< i + 1 <<" : "<< Wfrancais[i] <<" ---> " ;
            getline(cin, Wanglais2[i]);                       
        }
        std::cout << "=================="<<endl;
        std::cout << "r" << char(0x82) << "sultat : "<<endl;
        std::cout << "================= "<<endl;
        cin.ignore();
        for (int i = 0; i < 20; ++i){ 
            if(Wanglais2[i] != Wanglais[i]){
                error ++;
            }                  
            std::cout << Wfrancais[i] << " ---> "<<"\" "<<Wanglais2[i]<<" \""<< " R" << char(0x82) << "ponse : " << Wanglais[i] <<endl;
        }
        std::cout <<"=-=-=-=-=-=-=-=-=-=-=-=-=-="<<endl;
        std::cout <<"Nombre d'erreur : "<< error <<endl;
    }
}
