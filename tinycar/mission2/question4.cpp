#include <iostream>
#include <string>
#include <unistd.h>
using namespace std;

float nocarte(float ht,float tva, float tva2,int choix){
    if (choix == 1){
        if(ht>20000){
            float ttc =  ht*(1+tva)*0.9;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva);
            cout<< "le prix ttc : " << ttc <<endl;     
        }
    }
    else{
        if(ht>20000){
            float ttc = ht*(1+tva2)*0.9;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva2);
            cout<< "le prix ttc : " << ttc <<endl;     
        }
    }
    return 0;}

float platinium(float ht,float tva, float tva2,int choix){
    if (choix == 1){
        if(ht>20000){
            float ttc = ht*(1+tva)*0.9+0.85;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva);
            cout<< "le prix ttc : " << ttc <<endl;     
        }
    }
    else{
        if(ht>20000){
            float ttc = ht*(1+tva2)*0.9+0.85;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva2);
            cout<< "le prix ttc : " << ttc <<endl;     
        }
    }
    return 0;}

float gold(float ht,float tva, float tva2,int choix){
    if (choix == 1){
        if(ht>20000){
            float ttc = ht*(1+tva)*0.9+0.70;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva);
            cout<< "le prix ttc : " << ttc <<endl;  
        }
    }
    else{
        if(ht>20000){
            float ttc = ht*(1+tva2)*0.9+0.80;
            cout<< "le prix ttc : " << ttc <<endl;     
        }
        else{
            float ttc = ht*(1+tva2);
            cout<< "le prix ttc : " << ttc <<endl;     
        }
    }
    return 0;}

int main(){
    float ht;
    int age;
    int choix;
    int fidel;
    int rate;
    float tva = 0.055;
    float tva2 = 0.2;
    float ttc;
    string marque;
    string model;
    string mdp = "Padawan";
    cout << "entr" << char(0x82) << " le mot de passe : ";
    cin >> mdp;
    while (mdp != "Padawan"){  // verifications mdp en boucle tant qu'il n'est pas correct 
        cout << "###########################" << endl;
        cout << "# mot de passe incorect ! #" << endl;
        cout << "###########################" << endl;
        cout << "entr" << char(0x82) << " le mot de passe : ";
        cin >> mdp;
    }
        cout << "<<<===================================>>>" << endl;
        cout << "quelles carte avez vous ?( ne r" << char(0x82) << "ponder que par 1 , 2 ou 3 ):" << endl;
        cout << "1.sans carte" << endl;
        cout << "2.carte Gold" << endl;
        cout << "3.carte Platinium" << endl;
        cin >> rate;
        cout << "<<<===================================>>>" << endl;
        switch (rate) {       // choix du forfait
        case 1: // forfait 1
            cout << "vous avez pris l'options sans carte " << endl;
            cout << "entr" << char(0x82) << " votre age : " << endl;
            cin >> age;
            cout << "votre voiture est electrique ou non ? (ne repondre que par 1 ou 2)" << endl;
            cout << "1.oui" << endl;
            cout << "2.non" << endl;
            cin >> choix;
            cout << "entr" << char(0x82) << " le prix de la voiture (HT) : " << endl;
            cin >> ht;
            nocarte( ht, tva,  tva2, choix);
            break;
        //==========================================================
        case 2: // forfait 2
            cout << "vous avez pris l'options Carte Gold " << endl;
            cout << "entr" << char(0x82) << " votre age : " << endl;
            cin >> age;
            cout << "votre voiture est electrique ou non ? (ne repondre que par 1 ou 2)" << endl;
            cout << "1.oui" << endl;
            cout << "2.non" << endl;
            cin >> choix;
            cout << "entr" << char(0x82) << "  le prix de la voiture (HT) : " << endl;
            cin >> ht;
            gold( ht, tva,  tva2, choix);
            break;
        //==========================================================
        case 3: // forfait 3
            cout << "vous avez pris l'options Carte Platinium" << endl;
            cout << "entr" << char(0x82) << " votre age : " << endl;
            cin >> age;
            cout << "votre voiture est electrique ou non ? (ne repondre que par 1 ou 2)" << endl;
            cout << "1.oui" << endl;
            cout << "2.non" << endl;
            cin >> choix;
            cout << "entr" << char(0x82) << "  le prix de la voiture (HT) : " << endl;
            cin >> ht;
            platinium( ht, tva,  tva2, choix);
            break;
        //==========================================================
        default: // valeur pour le forfait incorect !
            cout << "#### la valeur entr" << char(0x82) << "e est incorect !" << endl;
            cout << "#### le programme ce stop ! " << endl;
            exit(0);
            break;
    }
}