#include <iostream>
#include <string>
using namespace std;

int main()
{

    // initialisations des variables 

    int age;
    float ht;
    float tva = 0.2;
    string marque;
    string model;

    //premiere demande de variable à lutilisateur 

    cout << "entré votre age : " << endl;
    cin >> age;
    cout << "entré le prix de la voiture (HT) : " << endl;
    cin >> ht;

    //choix en fonctions du prix ( réduction de 10%)

    if (ht > 20000)
    {
        //Si prix au dessus de 20000 euro 
        cout << ">>>> ================== <<<<" << endl;
        cout << ">>>> avec le prix de votre voiture vous avez 10% de réductions <<<<" << endl;
        cout << ">>>> ================== <<<<" << endl;
        cout << "entré la marque de la voiture : " << endl;
        cin >> marque;
        cout << "entré le model de la voiture  : " << endl;
        cin >> model;
        cout << ">>>> ================== <<<<" << endl;
        cout << "le prix HT : " << ht << " EURO" << endl;
        cout << "le prix TTC sans Réduction :" <<ht*(1+tva) << endl;
        cout << "le prix TTC avec Réduction :" << ht*(1+tva) * 0.9 << endl;
        cout << "valeur tva 20% " << endl;
        cout << "marque : " << marque << endl;
        cout << "model : " << model << endl;
        cout << "age : " << age << endl;
        cout << ">>>> ================== <<<<" << endl;
    }

    else
    {
        //si prix en dessous de 20000 EURO
        cout << ">>>> ================== <<<<" << endl;
        cout << "entré la marque de la voiture : " << endl;
        cin >> marque;
        cout << "entré le model de la voiture  : " << endl;
        cin >> model;
        cout << ">>>> ================== <<<<" << endl;
        cout << "le prix HT : " << ht << " EURO" << endl;
        cout << "le prix TTC sans Réduction :" <<ht*(1+tva)<< endl;
        cout << "valeur tva 20% "<< endl;
        cout << "marque : " << marque << endl;
        cout << "model : " << model << endl;
        cout << "age : " << age << endl;
    }
}
