#include <iostream>
using namespace std;

// les différent variables sont des int , double que l'on pourrait remplcer par float .
// [Montant HT] * (1+TVA  )= [Montant TTC]

int main()
{
    int ht = 10000;
    float tva = 0.2;

    cout <<"le prix HT : "<<ht<< " EUR"<<endl;
    cout <<"valeur tva 20%  "<< endl;
    cout <<"le prix TTC : "<< ht*(1+tva) <<" EUR" << endl;
}
