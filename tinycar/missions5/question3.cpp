#include <iostream>
#include <string>
using namespace std;

float CalculePrixTTC (int prix){
    float tva = 0.2;
    return prix * (1 + tva);
}

float affichage (float prix, string options,float prixttc){
    cout<<"le prix HT est : "<<prix <<" et l'options est : "<<options<<endl;
    cout<<"--> sont prix TTC est : "<<prixttc<<endl;
    return 0; 
}

int main()
{
    int n = 0;
    cout << "combient D'article dans votre panier : ";
    cin >> n;                                 // demande le nombre de colone des tableaux 
    float prix[n];                            // définitions du tableau des prix 
    string options[n];
    float PrixTTC[n];                      // définitions du tableau des options 
    cout << "donner l'options puis le prix qui vas avec(le prix suit l'options)" << endl;
    cout << "-----------" << endl;
    for (int i = 0; i < n; i++)                             // demande en boucle l'options et le prix le nombre de fois que l'utilisateur lui a demandé 
    {
        cin.clear();                                        //permet de vider le buffer de cin
        cin.ignore(20000, '\n');                            // permet de vider le buffer de cin 
        cout << " options num " << i + 1 << " : ";
        getline(cin, options[i]);                           // permet de prendre tout le string avec les espaces !
        cout << " prix num " << i + 1 << " : ";
        cin >> prix[i];
        cout << "-----------" << endl;
        PrixTTC[i] = CalculePrixTTC(prix[i]);
    }

    cout << "====================" << endl;
    float somme = 0;

    for (int i = 0; i < n; i++){                         // permet d'afficher les tableaux avec les options et les prix rentré par l'utilisateur  
        somme = somme + prix[i]; 
    }

    for (int i = 0; i< n; i++){
        affichage (prix[i],options[i],PrixTTC[i]);
    }

    float moyenPrix = somme/n ;
    int prixMin =  prix[0];
    int indiceMin = 0;
    int prixMax = prix[0];
    int indiceMax = 0;

    for(int i = 0; i < n; ++i)
    {
       if(prixMin > prix[i]){
           prixMin = prix[i];
           indiceMin = i;
        } 
    }

    for(int i = 0; i < n; ++i)
    {
       if(prixMax < prix[i]){
            prixMax = prix[i];
            indiceMax = i;
       }
    }

    cout<<endl;
    cout<<"la sommes des prix de vos options : "<<somme<<" Euros"<<endl;
    cout<<"le prix le plus petit est " << prixMin <<" pour l'options : " << options[indiceMin] <<endl;
    cout<<"le prix le plus grand est " << prixMax <<" pour l'options : " << options[indiceMax] << endl;
    cout<< "le prix moyen est : "<<moyenPrix<<endl;
    cout << "====================" << endl;
}
