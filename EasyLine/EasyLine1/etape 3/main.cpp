#include <iostream>
#include <string>
#include "voyageur.h"
using namespace std;

Voyageur::Voyageur(string nom, int age){
    while(nom.length()<2){
        cout<<"quelle est le nom du voyageur ?";
        getline(cin,nom);
    }
    while(age<1){
        cout << "Quel est votre age ?";
        cin>>age;
    }
    this->nom=nom;
    this->age=age;
}

Voyageur::Voyageur(){}
void Voyageur::afficherDonnees(){
    cout<<"Voyageur : "<< this->nom<<endl;
    cout<<"age : "<<this->age<<endl;
}

void Voyageur::setNom(string nom){
    while(nom.length()<2){
        cout<<"quel est le nom du voyageur ?";
        getline(cin,nom);
    }
    this->nom=nom;
}

string Voyageur::getNom(){
    return this->nom;
}

void Voyageur::setAge(int age){
    while(age<1){
        cout<<"Quel est votre age ?";
        cin>>age;
    }
    this->age=age;
}

int Voyageur::getAge(){
    return this->age;
}



int main(){

    Voyageur Voyageur1;
    Voyageur1.setNom("Mr.Frebourg");
    Voyageur1.setAge(30);
    Voyageur1.afficherDonnees();
    string nom;
    int age;

    cout<<"votre nom : ";
    getline(cin,nom);
    cout<<"votre age : ";
    cin>>age;
    Voyageur Voyageur2(nom,age);
    Voyageur2.afficherDonnees();

    if (age<=1){
        cout<<"nourrisson";
    }
    if (age>1 && age<18){
        cout<<"enfant";
    }
    if (age>18 && age<60){
        cout<<"adulte";
    }
    if (age>=60){
        cout<<"senior";
    }
}
