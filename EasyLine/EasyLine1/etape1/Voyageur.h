
#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
using namespace std;

class Voyageur{

public:
    string nom;
    int age;
    Voyageur(string nom = "VoyageurX", int age = 20);
    void afficherDonnees();

};

#endif 
