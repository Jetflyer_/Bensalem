
#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
using namespace std;

class Voyageur{
    private:
        string nom;
        int age;
    public:   
        Voyageur(string nom, int age);
        Voyageur();
        void afficherDonnees();

        void setNom(string nom);
        void setAge(int age);

        string getNom();
        int getAge();
};

#endif 
